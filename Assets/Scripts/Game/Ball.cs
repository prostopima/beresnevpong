﻿using System;
using System.Collections;
using System.Collections.Generic;
using SignalsFramework;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using Random = UnityEngine.Random;

public class Ball : MonoBehaviour
{
    public static Signal EvStart = new Signal();
    public static Signal EvBounce = new Signal();
    public static Signal EvOut = new Signal();

    public float maxAngle = 70f;
    public float fadeDuration = 0.3f;
    public Vector2 velocityRange;
    public Vector2 scaleRange;

    private Vector2 velocity;
    private SpriteRenderer sprite;
    
    void Start()
    {
        sprite = gameObject.GetComponent<SpriteRenderer>();
        OnStart();
    }

    private void OnStart()
    {
        //reset position
        transform.localPosition = Vector3.zero;
        
        //set random scale
        var scale = Random.Range(scaleRange.x, scaleRange.y);
        transform.localScale = new Vector3(scale, scale, 1);
        
        //random direction and velocity
        float angle = Random.Range(-maxAngle, maxAngle) * Mathf.Deg2Rad;
        velocity = new Vector2(Mathf.Sin(angle), Mathf.Cos(angle) * (Random.value > 0.5f ? 1 : -1))
                   * Random.Range(velocityRange.x, velocityRange.y);
        
        sprite.SetAlpha(1f);
        EvStart.Fire();
    }

    private void FixedUpdate()
    {
        Vector3 p = transform.localPosition;
        p += new Vector3(velocity.x * Time.fixedDeltaTime, velocity.y * Time.fixedDeltaTime, 0f);
        transform.localPosition = p;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        EvOut.Fire();

        //fade and restart
        float timeSpent = 0;
        Observable.EveryUpdate()
            .TakeWhile(_ => timeSpent < fadeDuration)
            .Subscribe(_ =>
                {
                    timeSpent += Time.deltaTime;
                    sprite.SetAlpha(1f - timeSpent / fadeDuration);
                },
                () => OnStart())
            .AddTo(this);
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        ContactPoint2D contact = other.contacts[other.contacts.Length - 1];
        
        velocity = Vector2.Reflect(velocity, contact.normal);

        //on racket collision
        Racket r = other.gameObject.GetComponent<Racket>();
        if (r != null)
        {
            Debug.Log(r.DeltaX+"  "+velocity);
            velocity = (new Vector2(r.DeltaX, 0) + velocity).normalized * velocity.magnitude;
            EvBounce.Fire();
        }
        
        //restrict angle
        velocity = ClamAngleAlongVerticalAxe(velocity);
    }

    private Vector2 ClamAngleAlongVerticalAxe(Vector2 v)
    {
        float sign = Mathf.Sign(v.y);
        v.y = Mathf.Abs(v.y);
        if (Vector2.Angle(Vector2.up, v) > maxAngle)
        {
            float angle = Vector2.SignedAngle(v, Vector2.up);
            angle = Mathf.Clamp(angle, -maxAngle, maxAngle)*Mathf.Deg2Rad;
            v = new Vector2(Mathf.Sin(angle), Mathf.Cos(angle)) * v.magnitude;
        }
        v.y *= sign;
        return v;
    }
}
