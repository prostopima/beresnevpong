﻿using System.Collections;
using System.Collections.Generic;
using SignalsFramework;
using UniRx;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public const string BEST_SCORE_LABEL = "best score";
    
    public static Signal EvStartRound = new Signal();
    public static Signal<int> EvBounce = new Signal<int>();

    public static int BestScore
    {
        get => PlayerPrefs.GetInt(BEST_SCORE_LABEL, 0);
        private set => PlayerPrefs.SetInt(BEST_SCORE_LABEL, value);
    }

    private int bounces;
    
    void Start()
    {
        Ball.EvStart.Subscribe(_ => OnRoundStart()).AddTo(this);
        Ball.EvBounce.Subscribe(_ => OnBounce()).AddTo(this);
        Ball.EvOut.Subscribe(_ => OnOut()).AddTo(this);
    }

    private void OnRoundStart()
    {
        bounces = 0;
        EvStartRound.Fire();
    }

    private void OnBounce()
    {
        bounces++;
        CheckBestScore();
        EvBounce.Fire(bounces);
    }

    private void OnOut()
    {
        CheckBestScore();
    }

    private void CheckBestScore()
    {
        if (BestScore < bounces)
            BestScore = bounces;
    }
}
