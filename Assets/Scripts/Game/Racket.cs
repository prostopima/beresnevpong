﻿using System.Collections;
using System.Collections.Generic;
using UniRx;
using UnityEngine;

public class Racket : MonoBehaviour
{
    public float DeltaX => deltaX;

    private float deltaX;

    private float width;
    private float bounds;
    private float containerWidth = 10;
    
    void Start()
    {
        width = transform.localScale.x;
        bounds = (containerWidth - width) / 2f;

        TouchController.EvMoved.Subscribe(OnMoved).AddTo(this);
        TouchController.EvMouseUp.Subscribe(_ => deltaX = 0).AddTo(this);
    }

    private void OnMoved(Vector3 deltaPosition)
    {
        Vector3 p = transform.localPosition;
        p.x += (containerWidth)*deltaPosition.x;
        p.x = Mathf.Clamp(p.x, -bounds, bounds);
        transform.localPosition = p;
        deltaX = deltaPosition.x/Time.deltaTime;
    }
}
