﻿using System.Collections;
using System.Collections.Generic;
using SignalsFramework;
using UniRx;
using UnityEngine;

public class TouchController : MonoBehaviour
{
    public static Signal<Vector3> EvMoved = new Signal<Vector3>(); 
    public static Signal EvMouseUp = new Signal(); 
    
    void Start()
    {
        Camera camera = Camera.main;

        var mouseDownStream = Observable.EveryUpdate().Where(_ => Input.GetMouseButtonDown(0));
        var mouseUpStream = Observable.EveryUpdate().Where(_ => Input.GetMouseButtonUp(0));

        //broadcast viewport delta mouse position
        Vector3 prevPosition = Vector3.zero;
        mouseDownStream.Subscribe(_ => prevPosition = camera.ScreenToViewportPoint(Input.mousePosition));
        mouseDownStream.SelectMany(_ => Observable.EveryUpdate())
            .TakeUntil(mouseUpStream)
            .Select(_ => camera.ScreenToViewportPoint(Input.mousePosition))
            .RepeatUntilDestroy(this)
            .Subscribe(position =>
            {
                EvMoved.Fire(position-prevPosition);
                prevPosition = position;
            })
            .AddTo(this);

        mouseUpStream.Subscribe(_ => EvMouseUp.Fire());
    }
}
