﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UniRx;
using UnityEngine;

public class GameUI : MonoBehaviour
{

    public TextMeshProUGUI scoreLabel;

    private int bestScore;
    
    void Start()
    {
        GameController.EvStartRound.Subscribe(_ => OnStartRound()).AddTo(this);
        GameController.EvBounce.Subscribe(OnBounce).AddTo(this);
    }

    private void OnStartRound()
    {
        bestScore = GameController.BestScore;
        scoreLabel.text = "0/" + bestScore;
    }

    private void OnBounce(int bounces)
    {
        scoreLabel.text = bounces+"/" + bestScore;
    }
    
}
