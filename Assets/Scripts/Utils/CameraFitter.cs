﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFitter : MonoBehaviour
{
    public float width = 10;
    
    void Awake()
    {
        gameObject.GetComponent<Camera>().orthographicSize =
            width / ((float) Screen.width / (float) Screen.height) / 2f;
    }

}
