﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Extentions
{

    public static void SetAlpha(this SpriteRenderer spriteRenderer, float alpha)
    {
        Color color = spriteRenderer.color;
        color.a = alpha;
        spriteRenderer.color = color;
    }
}
